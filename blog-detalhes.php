<?php 
require_once("inc/header.php");
?>

<section id="inner-page">
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell">
        <h1>Blog</h1>
        <ul class="breadcrumbs">
          <li><a href="#">Home</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">Quando devo trocar os pneus do meu carro?</a></li>
        </ul>        
      </div>
    </div>
  </div>
</section>

<section class="padding-top-3 padding-bottom-3 bg-grey">
    <div class="grid-container">

      <div class="grid-x grid-margin-x margin-top-1">

        <div class="large-8 medium-8 cell margin-bottom-3">
          <div id="blog-content">
          
          <h1>Quando devo trocar os pneus do meu carro?</h1>
          <h2 class="sub-title">A vida útil e a quilometragem de um pneu dependem de uma combinação de fatores: escultura, hábitos do motorista, clima, condições das vias e cuidados com os pneus.</h2>

          <img src="/img/blog1.jpg">

          <h3>1 - Tenha em mente o período de cinco anos</h3>
          <p>A partir de cinco anos de uso, os pneus devem passar por uma inspeção profissional minuciosa ao menos uma vez por ano.</p>

          <h3>2 - Dez anos é o máximo</h3>
          <p>Se não houve reposição dos pneus 10 anos após a data de fabricação, a Michelin recomenda a troca por pneus novos como precaução, mesmo que pareçam estar em condições de uso e o indicador de desgaste da banda de rodagem não tenha sido atingido. Isso também se aplica aos estepes.</p>


          <h3>3- A manutenção adequada aumenta a vida útil do pneu</h3>
          <p>A devida atenção à calibragem dos pneus, desgaste da banda de rodagem, alinhamento, etc., pode prolongar sua vida útil. Confira nossas dicas de manutenção agendada (link para a página Como faço a manutenção dos meus pneus)</p>

          <h3>Hábitos do motorista:</h3>
          <ul>
            <li>Excesso de velocidade</li>
            <li>Arranques rápidos e frenagem de emergência</li>
            <li>Circulação em vias mal conservadas</li>
            <li>Não observarção de alterações no comportamento, ruídos ou vibrações</li>
            <li>Falta de consulta a um profissional quando ocorre alguma alteração</li>
          </ul>

          </div>
        </div>     

        <div class="large-4 medium-6 cell margin-bottom-3">
          <h5 class="font-bold color-blue margin-bottom-1">Outros posts:</h5>
          <div class="card-blog margin-bottom-3">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog2.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>

          <div class="card-blog margin-bottom-3">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog3.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>          

          <div class="card-blog margin-bottom-3">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog1.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>

          <div class="text-center">
            <a class="button button-ghost" href="#">Confira todos os posts</a>
          </div>

        </div>  

      </div>



    </div>
</section>

<?php 
require_once("inc/footer.php");
?>