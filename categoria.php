<?php 
require_once("inc/header.php");
?>

<section id="inner-page">
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell">
        <h1>Nossos serviços</h1>
        <ul class="breadcrumbs">
          <li><a href="#">Home</a></li>
          <li><a href="#">Pneus</a></li>
          <li><a href="#">Pneus linha leve</a></li>
        </ul>        
      </div>
    </div>
  </div>
</section>

<section class="margin-top-3 margin-bottom-3">
  <div class="grid-container">
    <div class="grid-x grid-margin-x">
        <div class="large-3 medium-3 cell">
          <div class="card-filter">
            <h4>Tipo</h4>
            <div class="card-filter-content">
              <label><input type="checkbox" name="tipo" value="Carro"> Carro</label>
              <label><input type="checkbox" name="tipo" value="SUV"> SUV</label>
              <label><input type="checkbox" name="tipo" value="Utilitário"> Utilitário</label>
              <label><input type="checkbox" name="tipo" value="Motocicleta"> Motocicleta</label>
            </div>
          </div>

          <div class="card-filter">
            <h4>Marca</h4>
            <div class="card-filter-content">
              <label><input type="checkbox" name="tipo" value="Volkswagen"> Volkswagen</label>
              <label><input type="checkbox" name="tipo" value="Fiat"> Fiat</label>
              <label><input type="checkbox" name="tipo" value="Honda"> Honda</label>
              <label><input type="checkbox" name="tipo" value="Gm"> Gm</label>
              <label><input type="checkbox" name="tipo" value="Peugeot"> Peugeot</label>
              <label><input type="checkbox" name="tipo" value="Renault"> Renault</label>
              <label><input type="checkbox" name="tipo" value="Citroen"> Citroen</label>
              <label><input type="checkbox" name="tipo" value="Toyota"> Toyota</label>
              <label><input type="checkbox" name="tipo" value="Mitsubishi"> Mitsubishi</label>
              <label><input type="checkbox" name="tipo" value="Hyundai"> Hyundai</label>
              <label><input type="checkbox" name="tipo" value="Kia motors"> Kia motors</label>
              <label><input type="checkbox" name="tipo" value="Volkswagen"> Volkswagen</label>
              <label><input type="checkbox" name="tipo" value="Fiat"> Fiat</label>
              <label><input type="checkbox" name="tipo" value="Honda"> Honda</label>
              <label><input type="checkbox" name="tipo" value="Gm"> Gm</label>
              <label><input type="checkbox" name="tipo" value="Peugeot"> Peugeot</label>
              <label><input type="checkbox" name="tipo" value="Renault"> Renault</label>
              <label><input type="checkbox" name="tipo" value="Citroen"> Citroen</label>
              <label><input type="checkbox" name="tipo" value="Toyota"> Toyota</label>
              <label><input type="checkbox" name="tipo" value="Mitsubishi"> Mitsubishi</label>
              <label><input type="checkbox" name="tipo" value="Hyundai"> Hyundai</label>
              <label><input type="checkbox" name="tipo" value="Kia motors"> Kia motors</label>            
            </div>
          </div>

          <div class="card-filter">
            <h4>Modelo</h4>
            <div class="card-filter-content">
              <p>Selecione uma marca para visualizar os modelos disponíves.</p>
            </div>
          </div>

        </div>

        <div class="large-9 medium-9 cell">
          <div class="grid-x grid-margin-x">
            
            <div class="large-4 medium-6 cell margin-bottom-2">
              <div class="box-product">
                <div class="box-product-thumb"><img src="/img/product1.png" alt="{{name}}" title="{{name}}"></div>
                <div class="box-product-title"><h3>Goodyear Cargo Marathon 2</h3></div>
                <div class="box-product-info"><p>Composto especial com sílica na banda de rodagem e construção otimizada proporcionam menor resistência ao rolamento.</p></div>
                <div class="box-product-target"><img src="/img/icon-automovel.svg" alt="Automóvel" title="Automóvel"> Ideal para: automóveis</div>
              </div>              
            </div>

            <div class="large-4 medium-6 cell margin-bottom-2">
              <div class="box-product">
                <div class="box-product-thumb"><img src="/img/product2.png" alt="{{name}}" title="{{name}}"></div>
                <div class="box-product-title"><h3>Goodyear Wrangler SUV</h3></div>
                <div class="box-product-info"><p>Entrega maior durabilidade e controle através de seu design superior e construção otimizada. Melhor aderência em piso molhado.</p></div>
                <div class="box-product-target"><img src="/img/icon-suv.svg" alt="SUV/Utilitários" title="SUV/Utilitários"> Ideal para: SUV/Utilitários</div>
              </div>              
            </div>

            <div class="large-4 medium-6 cell margin-bottom-2">
              <div class="box-product">
                <div class="box-product-thumb"><img src="/img/product3.png" alt="{{name}}" title="{{name}}"></div>
                <div class="box-product-title"><h3>Kelly Edge SUV</h3></div>
                <div class="box-product-info"><p>Desenvolvido para Pick-ups e SUVs. Seu baixo desgaste da banda de rodagem entrega alta quilometragem e economia no seu dia-a-dia.</p></div>
                <div class="box-product-target"><img src="/img/icon-suv.svg" alt="SUV/Utilitários" title="SUV/Utilitários"> Ideal para: SUV/Utilitários</div>
              </div>              
            </div>

            <div class="large-4 medium-6 cell margin-bottom-2">
              <div class="box-product">
                <div class="box-product-thumb"><img src="/img/product1.png" alt="{{name}}" title="{{name}}"></div>
                <div class="box-product-title"><h3>G677 OTR</h3></div>
                <div class="box-product-info"><p>Transporta até 6 mil toneladas a mais. Recomendado para eixos de tração.</p></div>
                <div class="box-product-target"><img src="/img/icon-pesada.svg" alt="Linha pesada" title="Linha pesada"> Ideal para: Linha pesada</div>
              </div>              
            </div>

            <div class="large-4 medium-6 cell margin-bottom-2">
              <div class="box-product">
                <div class="box-product-thumb"><img src="/img/product1.png" alt="{{name}}" title="{{name}}"></div>
                <div class="box-product-title"><h3>Goodyear Cargo Marathon 2</h3></div>
                <div class="box-product-info"><p>Composto especial com sílica na banda de rodagem e construção otimizada proporcionam menor resistência ao rolamento.</p></div>
                <div class="box-product-target"><img src="/img/icon-automovel.svg" alt="Automóvel" title="Automóvel"> Ideal para: automóveis</div>
              </div>              
            </div>

            <div class="large-4 medium-6 cell margin-bottom-2">
              <div class="box-product">
                <div class="box-product-thumb"><img src="/img/product2.png" alt="{{name}}" title="{{name}}"></div>
                <div class="box-product-title"><h3>Goodyear Wrangler SUV</h3></div>
                <div class="box-product-info"><p>Entrega maior durabilidade e controle através de seu design superior e construção otimizada. Melhor aderência em piso molhado.</p></div>
                <div class="box-product-target"><img src="/img/icon-suv.svg" alt="SUV/Utilitários" title="SUV/Utilitários"> Ideal para: SUV/Utilitários</div>
              </div>              
            </div>

            <div class="large-4 medium-6 cell margin-bottom-2">
              <div class="box-product">
                <div class="box-product-thumb"><img src="/img/product3.png" alt="{{name}}" title="{{name}}"></div>
                <div class="box-product-title"><h3>Kelly Edge SUV</h3></div>
                <div class="box-product-info"><p>Desenvolvido para Pick-ups e SUVs. Seu baixo desgaste da banda de rodagem entrega alta quilometragem e economia no seu dia-a-dia.</p></div>
                <div class="box-product-target"><img src="/img/icon-suv.svg" alt="SUV/Utilitários" title="SUV/Utilitários"> Ideal para: SUV/Utilitários</div>
              </div>              
            </div>

            <div class="large-4 medium-6 cell margin-bottom-2">
              <div class="box-product">
                <div class="box-product-thumb"><img src="/img/product1.png" alt="{{name}}" title="{{name}}"></div>
                <div class="box-product-title"><h3>G677 OTR</h3></div>
                <div class="box-product-info"><p>Transporta até 6 mil toneladas a mais. Recomendado para eixos de tração.</p></div>
                <div class="box-product-target"><img src="/img/icon-pesada.svg" alt="Linha pesada" title="Linha pesada"> Ideal para: Linha pesada</div>
              </div>              
            </div>

            <div class="large-4 medium-6 cell margin-bottom-2">
              <div class="box-product">
                <div class="box-product-thumb"><img src="/img/product1.png" alt="{{name}}" title="{{name}}"></div>
                <div class="box-product-title"><h3>Goodyear Cargo Marathon 2</h3></div>
                <div class="box-product-info"><p>Composto especial com sílica na banda de rodagem e construção otimizada proporcionam menor resistência ao rolamento.</p></div>
                <div class="box-product-target"><img src="/img/icon-automovel.svg" alt="Automóvel" title="Automóvel"> Ideal para: automóveis</div>
              </div>              
            </div>

          </div>
        </div>

    </div>
  </div>
</section>


<?php 
require_once("inc/footer.php");
?>