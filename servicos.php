<?php 
require_once("inc/header.php");
?>

<section id="inner-page">
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell">
        <h1>Nossos serviços</h1>
        <ul class="breadcrumbs">
          <li><a href="#">Home</a></li>
          <li><a href="#">Nossos serviços</a></li>
        </ul>        
      </div>
    </div>
  </div>
</section>

<section class="margin-top-3 margin-bottom-3">
  <div class="grid-container">
    <div class="grid-x grid-margin-x">
      
      <div class="large-4 medium-4 cell margin-bottom-3">
        <a href="#">
          <div class="box-service">
              <img src="/img/higienizacao.jpg" alt="Higienização" title="Higienização" class="box-service-thumb">
              <div class="box-service-info">
                <div class="box-service-icon">
                  <img src="/img/higienizacao.svg" alt="Higienização" title="Higienização" class="box-service-icon">
                </div>
                <div class="box-service-label"><h2>Higienização do ar condicionado por Oxi Sanitização</h2></div>
              </div>
          </div>
        </a>        
      </div>

      <div class="large-4 medium-4 cell margin-bottom-3">
        <a href="#">
          <div class="box-service">
              <img src="/img/service2.jpg" alt="Serviço especializado de troca de óleo na Fredy" title="Serviço especializado de troca de óleo na Fredy" class="box-service-thumb">
              <div class="box-service-info">
                <div class="box-service-icon">
                  <img src="/img/service2.svg" alt="Serviço especializado de troca de óleo na Fredy" title="Serviço especializado de troca de óleo na Fredy" class="box-service-icon">
                </div>
                <div class="box-service-label"><h2>Serviço especializado de troca de óleo na Fredy</h2></div>
              </div>
          </div>
        </a>          
      </div>

      <div class="large-4 medium-4 cell margin-bottom-3">
        <a href="#">
          <div class="box-service">
              <img src="/img/service3.jpg" alt="Revisão e a manutenção da suspensão de veículos" title="Revisão e a manutenção da suspensão de veículos" class="box-service-thumb">
              <div class="box-service-info">
                <div class="box-service-icon">
                  <img src="/img/service3.svg" alt="Revisão e a manutenção da suspensão de veículos" title="Revisão e a manutenção da suspensão de veículos" class="box-service-icon">
                </div>
                <div class="box-service-label"><h2>Revisão e a manutenção da suspensão de veículos</h2></div>
              </div>
          </div>
        </a>          
      </div>

      <div class="large-4 medium-4 cell margin-bottom-3">
        <a href="#">
          <div class="box-service">
              <img src="/img/service4.jpg" alt="Atenção com os pneus desgastados irregularmente" title="Atenção com os pneus desgastados irregularmente" class="box-service-thumb">
              <div class="box-service-info">
                <div class="box-service-icon">
                  <img src="/img/service4.svg" alt="Atenção com os pneus desgastados irregularmente" title="Atenção com os pneus desgastados irregularmente" class="box-service-icon">
                </div>
                <div class="box-service-label"><h2>Atenção com os pneus desgastados irregularmente</h2></div>
              </div>
          </div>
        </a>        
      </div>

      <div class="large-4 medium-4 cell margin-bottom-3">
        <a href="#">
          <div class="box-service">
              <img src="/img/service5.jpg" alt="O serviço ideal para manter suas rodas sempre novas." title="O serviço ideal para manter suas rodas sempre novas." class="box-service-thumb">
              <div class="box-service-info">
                <div class="box-service-icon">
                  <img src="/img/service5.svg" alt="O serviço ideal para manter suas rodas sempre novas." title="O serviço ideal para manter suas rodas sempre novas." class="box-service-icon">
                </div>
                <div class="box-service-label"><h2>O serviço ideal para manter suas rodas sempre novas.</h2></div>
              </div>
          </div>
        </a>          
      </div>

      <div class="large-4 medium-4 cell margin-bottom-3">
        <a href="#">
          <div class="box-service">
              <img src="/img/service6.jpg" alt="Manutenção e cuidados necessários com os freios" title="Manutenção e cuidados necessários com os freios" class="box-service-thumb">
              <div class="box-service-info">
                <div class="box-service-icon">
                  <img src="/img/service6.svg" alt="Manutenção e cuidados necessários com os freios" title="Manutenção e cuidados necessários com os freios" class="box-service-icon">
                </div>
                <div class="box-service-label"><h2>Manutenção e cuidados necessários com os freios</h2></div>
              </div>
          </div>
        </a>         
      </div>

      <div class="large-4 medium-4 cell margin-bottom-3">
        <a href="#">
          <div class="box-service">
              <img src="/img/service7.jpg" alt="Alinhamento e balanceamento de Pneus" title="Alinhamento e balanceamento de Pneus" class="box-service-thumb">
              <div class="box-service-info">
                <div class="box-service-icon">
                  <img src="/img/service7.svg" alt="Alinhamento e balanceamento de Pneus" title="Alinhamento e balanceamento de Pneus" class="box-service-icon">
                </div>
                <div class="box-service-label"><h2>Alinhamento e balanceamento de Pneus</h2></div>
              </div>
          </div>
        </a>        
      </div>

      <div class="large-4 medium-4 cell margin-bottom-3">
        <a href="#">
          <div class="box-service">
              <img src="/img/service8.jpg" alt="Conserto de Pneus" title="Conserto de Pneus" class="box-service-thumb">
              <div class="box-service-info">
                <div class="box-service-icon">
                  <img src="/img/service8.svg" alt="Conserto de Pneus" title="Conserto de Pneus" class="box-service-icon">
                </div>
                <div class="box-service-label"><h2>Conserto de Pneus</h2></div>
              </div>
          </div>
        </a>             
      </div>

      <div class="large-4 medium-4 cell margin-bottom-3">
        <a href="#">
          <div class="box-service">
              <img src="/img/service9.jpg" alt="Mantenha o seu carro sempre bem equilibrado" title="Mantenha o seu carro sempre bem equilibrado" class="box-service-thumb">
              <div class="box-service-info">
                <div class="box-service-icon">
                  <img src="/img/service9.svg" alt="Mantenha o seu carro sempre bem equilibrado" title="Mantenha o seu carro sempre bem equilibrado" class="box-service-icon">
                </div>
                <div class="box-service-label"><h2>Mantenha o seu carro sempre bem equilibrado</h2></div>
              </div>
          </div>
        </a>          
      </div>

    </div>
  </div>
</section>

<?php 
require_once("inc/footer.php");
?>