<?php 
require_once("inc/header.php");
?>

  <section>
    <div class="swiper-container swiper1">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <div><a href="#" class="hide-for-small-only"><img src="/img/slider1.jpg" alt="{{name}}" title="{{name}}"></a></div>
          <div class="show-for-small-only"><a href="#"><img src="/img/slider1-small.jpg" alt="{{name}}" title="{{name}}"></a></div>
        </div>                                                          
      </div>

      <div class="swiper-pagination swiper-pagination1"></div>
      <div class="swiper-button-next swiper-button-next1"><img src="/img/right.svg" alt="Right" title="Avançar"></div>
      <div class="swiper-button-prev swiper-button-prev1"><img src="/img/left.svg" alt="Left" title="Voltar"></div>
    </div>

    <div class="grid-container">
      <div class="grid-x grid-margin-x">

        <div class="large-6 medium-6 cell">

          <div class="box-shortcut">
            <div class="box-shortcut-category-name bg-blue">
              <h2>PNEUS</h2>
              <a href="#">Ver todos</a>
            </div>
            <div class="box-shortcut-category-common">
              <a href="#">
                <div class="box-shortcut-category-thumb"><img src="/img/icon-automovel.svg" alt="Automóvel" title="Automóvel"></div>
                <div class="box-shortcut-category-label">Automóvel</div>
              </a>
            </div>
            <div class="box-shortcut-category-common">
              <a href="#">
                <div class="box-shortcut-category-thumb"><img src="/img/icon-suv.svg" alt="SUV/Utilitários" title="SUV/Utilitários"></div>
                <div class="box-shortcut-category-label">SUV/Utilitários</div>
              </a>              
            </div>
            <div class="box-shortcut-category-common">
              <a href="#">
                <div class="box-shortcut-category-thumb"><img src="/img/icon-pesada.svg" alt="Linha pesada" title="Linha pesada"></div>
                <div class="box-shortcut-category-label">Linha pesada</div>
              </a>               
            </div>
          </div>

        </div>

        <div class="large-6 medium-6 cell">
          
          <div class="box-shortcut">
            <div class="box-shortcut-category-name bg-green">
              <h2>RODAS</h2>
              <a href="#">Ver todos</a>
            </div>
            <div class="box-shortcut-category-common">
              <a href="#">
                <div class="box-shortcut-category-thumb"><img src="/img/icon-rodas-esportivas.svg" alt="Esportivas" title="Esportivas"></div>
                <div class="box-shortcut-category-label">Esportivas</div>
              </a>
            </div>
            <div class="box-shortcut-category-common">
              <a href="#">
                <div class="box-shortcut-category-thumb"><img src="/img/icon-rodas-originais.svg" alt="Originais" title="Originais"></div>
                <div class="box-shortcut-category-label">Originais</div>
              </a>              
            </div>
            <div class="box-shortcut-category-common">
              <a href="#">
                <div class="box-shortcut-category-thumb"><img src="/img/icon-rodas-seminovas.svg" alt="Semi-novas" title="Semi-novas"></div>
                <div class="box-shortcut-category-label">Semi-novas</div>
              </a>               
            </div>
          </div>

        </div>

      </div>
    </div>

  </section>     

  <section class="margin-top-3">
    <div class="grid-container padding-right-0-mobile">
      <div class="grid-x">
        <div class="cell">
          <h2 class="title-default">Serviços</h2>
          <h2 class="sub-title">Oferecemos uma gama completa de serviços especializados para você. Confira:</h2>
        </div>
      </div>

      <div class="grid-x margin-top-1">

        <div class="cell relative">
          <div class="swiper-button-next swiper-button-next2"><img src="/img/right2.svg" alt="Right" title="Avançar"></div>
          <div class="swiper-button-prev swiper-button-prev2"><img src="/img/left2.svg" alt="Left" title="Voltar"></div>

          <div class="swiper-container swiper2">
            <div class="swiper-wrapper">
              
              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/higienizacao.jpg" alt="Higienização" title="Higienização" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/higienizacao.svg" alt="Higienização" title="Higienização" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Higienização do ar condicionado por Oxi Sanitização</h2></div>
                      </div>
                  </div>
                </a>              
              </div>              

              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service2.jpg" alt="Serviço especializado de troca de óleo na Fredy" title="Serviço especializado de troca de óleo na Fredy" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service2.svg" alt="Serviço especializado de troca de óleo na Fredy" title="Serviço especializado de troca de óleo na Fredy" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Serviço especializado de troca de óleo na Fredy</h2></div>
                      </div>
                  </div>
                </a>              
              </div>

              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service3.jpg" alt="Revisão e a manutenção da suspensão de veículos" title="Revisão e a manutenção da suspensão de veículos" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service3.svg" alt="Revisão e a manutenção da suspensão de veículos" title="Revisão e a manutenção da suspensão de veículos" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Revisão e a manutenção da suspensão de veículos</h2></div>
                      </div>
                  </div>
                </a>              
              </div>              
                    
              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service4.jpg" alt="Atenção com os pneus desgastados irregularmente" title="Atenção com os pneus desgastados irregularmente" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service4.svg" alt="Atenção com os pneus desgastados irregularmente" title="Atenção com os pneus desgastados irregularmente" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Atenção com os pneus desgastados irregularmente</h2></div>
                      </div>
                  </div>
                </a>              
              </div> 

              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service5.jpg" alt="O serviço ideal para manter suas rodas sempre novas." title="O serviço ideal para manter suas rodas sempre novas." class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service5.svg" alt="O serviço ideal para manter suas rodas sempre novas." title="O serviço ideal para manter suas rodas sempre novas." class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>O serviço ideal para manter suas rodas sempre novas.</h2></div>
                      </div>
                  </div>
                </a>              
              </div>

              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service6.jpg" alt="Manutenção e cuidados necessários com os freios" title="Manutenção e cuidados necessários com os freios" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service6.svg" alt="Manutenção e cuidados necessários com os freios" title="Manutenção e cuidados necessários com os freios" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Manutenção e cuidados necessários com os freios</h2></div>
                      </div>
                  </div>
                </a>              
              </div> 

              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service7.jpg" alt="Alinhamento e balanceamento de Pneus" title="Alinhamento e balanceamento de Pneus" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service7.svg" alt="Alinhamento e balanceamento de Pneus" title="Alinhamento e balanceamento de Pneus" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Alinhamento e balanceamento de Pneus</h2></div>
                      </div>
                  </div>
                </a>              
              </div>

              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service8.jpg" alt="Conserto de Pneus" title="Conserto de Pneus" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service8.svg" alt="Conserto de Pneus" title="Conserto de Pneus" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Conserto de Pneus</h2></div>
                      </div>
                  </div>
                </a>              
              </div>

              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service9.jpg" alt="Mantenha o seu carro sempre bem equilibrado" title="Mantenha o seu carro sempre bem equilibrado" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service9.svg" alt="Mantenha o seu carro sempre bem equilibrado" title="Mantenha o seu carro sempre bem equilibrado" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Mantenha o seu carro sempre bem equilibrado</h2></div>
                      </div>
                  </div>
                </a>              
              </div>                    

            </div>
          </div> 

        </div>

      </div>

      <div class="grid-x margin-top-2 text-center">
        <div class="cell margin-top-1 margin-bottom-1">
          <a class="button button-ghost" href="#">Confira todos os serviços</a>
        </div>
      </div>

    </div>
  </section>

  <section class="margin-top-2">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell">
          <a href="#">
            <img src="/img/encontre-a-proxima-unidade.png" alt="Encontre a próxima unidade" title="Encontre a próxima unidade">
          </a>
        </div>
      </div>
    </div>
  </section>

  <section class="margin-top-2">
    <div class="grid-container padding-right-0-mobile">
      <div class="grid-x">
        <div class="cell">
          <div class="flex-icon-title">
            <div class="flex-icon-title-icon"><img src="/img/icon-oferta.svg" alt="Ofertas" title="Ofertas"></div>
            <div class="flex-icon-title-label">
              <h2 class="title-default">Nossa linha de produtos</h2>
              <h2 class="sub-title">Temos os melhores produtos para você.</h2>          
            </div>
          </div>
        </div>
      </div>

      <div class="grid-x grid-margin-x">

        <div class="cell relative">
          <div class="swiper-button-next swiper-button-next3"><img src="/img/right2.svg" alt="Right" title="Avançar"></div>
          <div class="swiper-button-prev swiper-button-prev3"><img src="/img/left2.svg" alt="Left" title="Voltar"></div>

          <div class="swiper-container swiper3 padding-top-2">
            <div class="swiper-wrapper">
              
              <div class="swiper-slide">
                <div class="box-product">
                  <div class="box-product-thumb"><img src="/img/product1.png" alt="{{name}}" title="{{name}}"></div>
                  <div class="box-product-title"><h3>Goodyear Cargo Marathon 2</h3></div>
                  <div class="box-product-info"><p>Composto especial com sílica na banda de rodagem e construção otimizada proporcionam menor resistência ao rolamento.</p></div>
                  <div class="box-product-target"><img src="/img/icon-automovel.svg" alt="Automóvel" title="Automóvel"> Ideal para: automóveis</div>
                </div>
              </div> 

              <div class="swiper-slide">
                <div class="box-product">
                  <div class="box-product-thumb"><img src="/img/product2.png" alt="{{name}}" title="{{name}}"></div>
                  <div class="box-product-title"><h3>Goodyear Wrangler SUV</h3></div>
                  <div class="box-product-info"><p>Entrega maior durabilidade e controle através de seu design superior e construção otimizada. Melhor aderência em piso molhado.</p></div>
                  <div class="box-product-target"><img src="/img/icon-suv.svg" alt="SUV/Utilitários" title="SUV/Utilitários"> Ideal para: SUV/Utilitários</div>
                </div>
              </div> 

              <div class="swiper-slide">
                <div class="box-product">
                  <div class="box-product-thumb"><img src="/img/product3.png" alt="{{name}}" title="{{name}}"></div>
                  <div class="box-product-title"><h3>Kelly Edge SUV</h3></div>
                  <div class="box-product-info"><p>Desenvolvido para Pick-ups e SUVs. Seu baixo desgaste da banda de rodagem entrega alta quilometragem e economia no seu dia-a-dia.</p></div>
                  <div class="box-product-target"><img src="/img/icon-suv.svg" alt="SUV/Utilitários" title="SUV/Utilitários"> Ideal para: SUV/Utilitários</div>
                </div>
              </div> 
              
              <div class="swiper-slide">
                <div class="box-product">
                  <div class="box-product-thumb"><img src="/img/product1.png" alt="{{name}}" title="{{name}}"></div>
                  <div class="box-product-title"><h3>G677 OTR</h3></div>
                  <div class="box-product-info"><p>Transporta até 6 mil toneladas a mais. Recomendado para eixos de tração.</p></div>
                  <div class="box-product-target"><img src="/img/icon-pesada.svg" alt="Linha pesada" title="Linha pesada"> Ideal para: Linha pesada</div>
                </div>
              </div> 

            </div>
          </div>          
        </div>

      </div>

      <div class="grid-x margin-top-2 text-center">
        <div class="cell">
          <a class="button button-ghost" href="#">Confira todos os serviços</a>
        </div>
      </div>      

    </div>
  </section>

  <section class="margin-top-3">
      <div class="background-app">
        <div class="grid-container">
            <div class="grid-x">
              <div class="large-4 medium-3 text-center cell">
                <img src="/img/icon-app.png" alt="App" title="App">
              </div>
              <div class="large-8 medium-8 cell">
                <h2>Conheça o nosso aplicativo</h2>
                
                <div class="feature-app margin-top-1">
                  <div class="feature-app-icon"><img src="/img/icon-app1.svg" alt="App" title="App"></div>
                  <div class="feature-app-label">Visualize sua próxima manutenção acessando <br>o QR Code em seu veículo! </div>
                </div>

                <div class="feature-app">
                  <div class="feature-app-icon"><img src="/img/icon-app2.svg" alt="App" title="App"></div>
                  <div class="feature-app-label">VProdutos e serviços exclusivos Fredy Pneus<br> para o seu carro</div>
                </div>

                <div class="feature-app">
                  <div class="feature-app-icon"><img src="/img/icon-app3.svg" alt="App" title="App"></div>
                  <div class="feature-app-label">Controle todas as suas revisões com o seu celular e <br>muito mais disponíveis aqui no nosso super app!</div>
                </div>

                <div class="cell">
                  <a href="#" class="button button-fill">Saiba mais sobre o aplicativo</a>
                </div>

              </div>
            </div>
        </div>
      </div>
  </section>

  <section class="margin-top-3 onde-estamos">
    <div class="grid-container">
      <div class="grid-x grid-margin-x">
        <div class="large-6 medium-12 cell">
          <h2>Onde estamos?</h2>
          <h3>Clique sobre uma unidade para saber mais.</h3>

          <div class="wrapper-location active" data-photo="unidade1.jpg" data-phone="(47) 2105-9800">
            <div class="wrapper-location-label"><h4>Joinville Matriz</h4></div>
          </div>

          <div class="wrapper-location" data-photo="unidade2.jpg" data-phone="(47) 2105-9800">
            <div class="wrapper-location-label"><h4>Joinville América</h4></div>
          </div>     

          <div class="wrapper-location" data-photo="unidade3.jpg" data-phone="(47) 2105-9800">
            <div class="wrapper-location-label"><h4>Joinville Iriríu</h4></div>
          </div>               

          <div class="wrapper-location" data-photo="unidade4.jpg" data-phone="(47) 2105-9800">
            <div class="wrapper-location-label"><h4>Jaraguá do Sul</h4></div>
          </div>

          <div class="wrapper-location" data-photo="unidade5.jpg" data-phone="(47) 2105-9800">
            <div class="wrapper-location-label"><h4>Brusque</h4></div>
          </div>               

          <div class="wrapper-location" data-photo="unidade6.jpg" data-phone="0800 006 2800">
            <div class="wrapper-location-label"><h4>Balneário Camboriú</h4></div>
          </div>             

          <div class="wrapper-location" data-photo="unidade7.jpg" data-phone="(47) 2105-9800">
            <div class="wrapper-location-label"><h4>São Bento do Sul</h4></div>
          </div> 

          <div class="wrapper-location" data-photo="unidade8.jpg" data-phone="0800 006 2800">
            <div class="wrapper-location-label"><h4>Itajaí</h4></div>
          </div>   

          <div class="wrapper-location" data-photo="unidade9.jpg" data-phone="0800 006 2800">
            <div class="wrapper-location-label"><h4>Florianópolis</h4></div>
          </div>

        </div>

        <div class="large-6 medium-12 cell">
          <div class="wrapper-photo-unidade">
            <img src="/img/unidade1.jpg" alt="Unidade" title="Unidade">
            <div class="unidade-telefone">
              <a href="#">
                <i class="fas fa-phone"></i> <span>(47) 2105-9800</span>
              </a>
            </div>

            <div class="como-chegar">
              <a href="/unidades">
                Saiba como chegar
              </a>
            </div>
          </div>
        </div>

      </div>

      <div class="grid-x margin-top-3">
        <div class="cell text-center"><a href="#" class="button button-yellow"><i class="fas fa-map-marker-alt"></i> Encontre a unidade mais próxima de você</a></div>
      </div>

    </div>
  </section>

  <section class="blog-section">
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell relative">
          <img src="/img/blog.svg" alt="Blog" title="Blog" class="blog-absolute">
          <h2 class="title-default">Dicas da Fredy</h2>
          <h2 class="sub-title">Fique por dentro das novidades em nosso blog</h2>
        </div>
      </div>

      <div class="grid-x grid-margin-x margin-top-1">

        <div class="large-4 medium-6 cell">
          <div class="card-blog">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog1.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>
        </div>     

        <div class="large-4 medium-6 cell">
          <div class="card-blog">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog2.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>
        </div>  

        <div class="large-4 medium-6 cell">
          <div class="card-blog">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog3.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>
        </div>

      </div>

      <div class="grid-x margin-top-3 text-center">
        <div class="cell margin-top-2">
          <a class="button button-ghost" href="#">Confira todas as novidades</a>
        </div>
      </div>

    </div>
  </section>

<?php 
require_once("inc/footer.php");
?>