<?php 
require_once("inc/header.php");
?>

<section id="inner-page">
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell">
        <h1>Contato</h1>
        <ul class="breadcrumbs">
          <li><a href="#">Home</a></li>
          <li><a href="#">Serviços</a></li>
          <li><a href="#">Contato</a></li>
        </ul>        
      </div>
    </div>
  </div>
</section>  
<section class="padding-top-1 padding-bottom-3">
  <div class="grid-container">
  
    <div class="grid-x grid-margin-x">
        <div class="cell">
          <ul class="breadcrumbs">
            <li><a href="#"><i class="fas fa-home"></i> Home</a></li>
            <li><a href="#">Contato</a></li>
          </ul>              
        </div>
        <div class="large-6 medium-6 cell">

        <form data-abide novalidate id="form" method="post" action="/contato">
          <div class="grid-margin-x grid-x">

            <div class="medium-12 cell">
              <label>Assunto
                <select name="objetivo" required>
                  <option value="">Selecione</option>
                  <option value="1">Comercial</option>
                  <option value="3">Ouvidoria</option>
                </select>
              </label>
            </div>

            <div class="medium-12 cell">
              <label>Nome
                <input name="nome" type="text" placeholder="Informe seu nome" required>
              </label>
            </div>

            <div class="medium-12 cell">
              <label>E-mail
                <input name="email" type="email" placeholder="Informe seu e-mail" required>
              </label>
            </div>

            <div class="medium-12 cell">
              <label>Telefone
                <input name="telefone" type="text" placeholder="Telefone para contato" required>
              </label>
            </div>

            <div class="medium-12 cell">
              <label>Mensagem
                <textarea name="mensagem" rows="8" placeholder="Digite sua mensagem" required></textarea>
              </label>
            </div>

            <div class="medium-6 cell text-left">
             RECAPTCHA
            </div>

            <div class="medium-6 cell text-right">
              <button type="submit" class="button more-detail">Enviar</button>
            </div>

          </div>
        </form>

        </div>  
        <div class="large-6 medium-6 cell">
          <h4 class="color-blue font-bold">Contatos</h5>
          <ul class="no-bullet">
            <li><a href="#" class="button button-fale-conosco"><img src="/img/icon-telemarketing.svg" alt="Fale conosco" title="Fale conosco"> Fale conosco</a></li>
            <li>Rua Cel Francisco Ribas, 650 Centro - Ponta Grossa - PR</li>
          </ul>

        </div>

    </div>

</section>



<?php 
require_once("inc/footer.php");
?>