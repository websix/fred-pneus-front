<?php 
require_once("inc/header.php");
?>

<section id="inner-page">
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell">
        <h1>Lojas</h1>
        <ul class="breadcrumbs">
          <li><a href="#">Home</a></li>
          <li><a href="#">Serviços</a></li>
          <li><a href="#">Lojas</a></li>
        </ul>        
      </div>
    </div>
  </div>
</section>

<section class="margin-top-3 margin-bottom-3">
  <div class="grid-container">
    <div class="grid-x grid-margin-x">
      <div class="cell show-for-large margin-top-1"><h5><strong class="color-blue">Joinville</strong></h5></div>       

      <div class="large-4 medium-6 cell margin-bottom-2">
        <div class="box-unidades">
          <div class="box-unidades-thumb">
            <img src="/img/unidade1.jpg" alt="Unidade {{name}}" title="Unidade {{name}}">
          </div>
          <div class="box-unidades-label">
            <h4>Joinville Matriz</h4>
            <h5>R. Sete de Setembro, 214 - Centro</h5>
          </div>
          <div class="box-unidades-info">
            
            <div class="box-unidades-info-flex">
              <div><a href="tel:4721059800"><i class="fas fa-phone"></i> (47) 2105-9800</a></div>
            </div>

            <div class="box-unidades-horario">
              <div><i class="far fa-clock"></i> Segunda a sexta: das 08:00h às 19:00h</div>
              <div><i class="far fa-clock"></i> Sábado das 08:00h às 13:00</div>
            </div>

            <div class="text-center">
              <a target="_blank" class="button button-ghost" href="https://goo.gl/maps/6782f6RjgHJAsa4t7">Saiba como chegar</a>
            </div>            
          </div>
        </div>         
      </div>

      <div class="large-4 medium-6 cell margin-bottom-2">
        <div class="box-unidades">
          <div class="box-unidades-thumb">
            <img src="/img/unidade2.jpg" alt="Unidade {{name}}" title="Unidade {{name}}">
          </div>
          <div class="box-unidades-label">
            <h4>Joinville América</h4>
            <h5>R. Dr. João Colin, 2620 - América</h5>
          </div>
          <div class="box-unidades-info">
            
            <div class="box-unidades-info-flex">
              <div><a href="tel:4721059800"><i class="fas fa-phone"></i> (47) 2105-9800</a></div>
            </div>

            <div class="box-unidades-horario">
              <div><i class="far fa-clock"></i> Segunda a sexta: das 08:00h às 19:00h</div>
              <div><i class="far fa-clock"></i> Sábado das 08:00h às 13:00</div>
            </div>

            <div class="text-center">
              <a target="_blank" class="button button-ghost" href="https://goo.gl/maps/6wVvLS4Jw3Dra1Yg7">Saiba como chegar</a>
            </div>            

          </div>
        </div>         
      </div>

      <div class="large-4 medium-6 cell margin-bottom-2">
        <div class="box-unidades">
          <div class="box-unidades-thumb">
            <img src="/img/unidade3.jpg" alt="Unidade {{name}}" title="Unidade {{name}}">
          </div>
          <div class="box-unidades-label">
            <h4>Joinville Iririú</h4>
            <h5>R. Guaíra, 575 - Iririú, Joinville - SC, 89225-300</h5>
          </div>
          <div class="box-unidades-info">
            
            <div class="box-unidades-info-flex">
              <div><a href="tel:4721059800"><i class="fas fa-phone"></i> (47) 2105-9800</a></div>
            </div>

            <div class="box-unidades-horario">
              <div><i class="far fa-clock"></i> Segunda a sexta: das 08:00h às 19:00h</div>
              <div><i class="far fa-clock"></i> Sábado das 08:00h às 13:00</div>
            </div>

            <div class="text-center">
              <a target="_blank" class="button button-ghost" href="https://goo.gl/maps/Dn5nGqgrdKtdb5ed9">Saiba como chegar</a>
            </div>            

          </div>
        </div>         
      </div>

      <div class="large-4 medium-6 cell margin-bottom-2">
        <div class="cell show-for-large margin-top-1"><h5><strong class="color-blue">Jaguará do Sul</strong></h5></div>
        <div class="box-unidades">
          <div class="box-unidades-thumb">
            <img src="/img/unidade4.jpg" alt="Unidade {{name}}" title="Unidade {{name}}">
          </div>
          <div class="box-unidades-label">
            <h4>Jaraguá do Sul</h4>
            <h5>R. Reinoldo Rau, 327 - Centro, Jaraguá do Sul - SC, 89251-600, Brasil</h5>
          </div>
          <div class="box-unidades-info">
            
            <div class="box-unidades-info-flex">
              <div><a href="tel:4721059800"><i class="fas fa-phone"></i> (47) 2105-9800</a></div>
            </div>

            <div class="box-unidades-horario">
              <div><i class="far fa-clock"></i> Segunda a sexta: das 08:00h às 19:00h</div>
              <div><i class="far fa-clock"></i> Sábado das 08:00h às 13:00</div>
            </div>

            <div class="text-center">
              <a target="_blank" class="button button-ghost" href="https://goo.gl/maps/QcrB2kp8xC6eqY1E8">Saiba como chegar</a>
            </div>            

          </div>
        </div>         
      </div>

      <div class="large-4 medium-6 cell margin-bottom-2">
        <div class="cell show-for-large margin-top-1"><h5><strong class="color-blue">Brusque</strong></h5></div>
        <div class="box-unidades">
          <div class="box-unidades-thumb">
            <img src="/img/unidade5.jpg" alt="Unidade {{name}}" title="Unidade {{name}}">
          </div>
          <div class="box-unidades-label">
            <h4>Brusque</h4>
            <h5>Av. Otto Renaux, 445 - São Luiz, Brusque - SC, 88351-301</h5>
          </div>
          <div class="box-unidades-info">
            
            <div class="box-unidades-info-flex">
              <div><a href="tel:08000062800"><i class="fas fa-phone"></i> 0800 006 2800</a></div>
            </div>

            <div class="box-unidades-horario">
              <div><i class="far fa-clock"></i> Segunda a sexta: das 08:00h às 19:00h</div>
              <div><i class="far fa-clock"></i> Sábado das 08:00h às 13:00</div>
            </div>

            <div class="text-center">
              <a target="_blank" class="button button-ghost" href="https://goo.gl/maps/K3D36Y6Z3bJQzLtHA">Saiba como chegar</a>
            </div>            

          </div>
        </div>         
      </div>

      <div class="large-4 medium-6 cell margin-bottom-2">
        <div class="cell show-for-large margin-top-1"><h5><strong class="color-blue">Balneário Camburiú</strong></h5></div>
        <div class="box-unidades">
          <div class="box-unidades-thumb">
            <img src="/img/unidade6.jpg" alt="Unidade {{name}}" title="Unidade {{name}}">
          </div>
          <div class="box-unidades-label">
            <h4>Balneário Camburiú</h4>
            <h5>Av. do Estado Dalmo Vieira, 1934 - Ariribá, Balneário Camboriú - SC, 88338-640</h5>
          </div>
          <div class="box-unidades-info">
            
            <div class="box-unidades-info-flex">
              <div><a href="tel:08000062800"><i class="fas fa-phone"></i> 0800 006 2800</a></div>
            </div>

            <div class="box-unidades-horario">
              <div><i class="far fa-clock"></i> Segunda a sexta: das 08:00h às 19:00h</div>
              <div><i class="far fa-clock"></i> Sábado das 08:00h às 13:00</div>
            </div>

            <div class="text-center">
              <a target="_blank" class="button button-ghost" href="https://goo.gl/maps/xvmpLpuGmY73BVFC7">Saiba como chegar</a>
            </div>            

          </div>
        </div>         
      </div>     

      <div class="large-4 medium-6 cell margin-bottom-2">
        <div class="cell show-for-large margin-top-1"><h5><strong class="color-blue">São Bento do Sul</strong></h5></div>
        <div class="box-unidades">
          <div class="box-unidades-thumb">
            <img src="/img/unidade7.jpg" alt="Unidade {{name}}" title="Unidade {{name}}">
          </div>
          <div class="box-unidades-label">
            <h4>São Bento do Sul</h4>
            <h5>R. Antônio Kaesemodel, 325 - Rio Negro, São Bento do Sul - SC, 89290-000</h5>
          </div>
          <div class="box-unidades-info">
            
            <div class="box-unidades-info-flex">
              <div><a href="tel:4721059800"><i class="fas fa-phone"></i> (47) 2105-9800</a></div>
            </div>

            <div class="box-unidades-horario">
              <div><i class="far fa-clock"></i> Segunda a sexta: das 08:00h às 19:00h</div>
              <div><i class="far fa-clock"></i> Sábado das 08:00h às 13:00</div>
            </div>

            <div class="text-center">
              <a target="_blank" class="button button-ghost" href="https://goo.gl/maps/7t6U2mtdUtuQK4jE6">Saiba como chegar</a>
            </div>            

          </div>
        </div>         
      </div>      


    <div class="large-4 medium-6 cell margin-bottom-2">
        <div class="cell show-for-large margin-top-1"><h5><strong class="color-blue">Itajaí</strong></h5></div>
        <div class="box-unidades">
          <div class="box-unidades-thumb">
            <img src="/img/unidade8.jpg" alt="Unidade {{name}}" title="Unidade {{name}}">
          </div>
          <div class="box-unidades-label">
            <h4>Itajaí</h4>
            <h5>Av. Irineu Bornhausen, 443 - São João, Itajaí - SC, 88305-000</h5>
          </div>
          <div class="box-unidades-info">
            
            <div class="box-unidades-info-flex">
              <div><a href="tel:08000062800"><i class="fas fa-phone"></i> 0800 006 2800</a></div>
            </div>

            <div class="box-unidades-horario">
              <div><i class="far fa-clock"></i> Segunda a sexta: das 08:00h às 19:00h</div>
              <div><i class="far fa-clock"></i> Sábado das 08:00h às 13:00</div>
            </div>

            <div class="text-center">
              <a target="_blank" class="button button-ghost" href="https://goo.gl/maps/h7yeksCEGt7kh7m69">Saiba como chegar</a>
            </div>            

          </div>
        </div>         
      </div>

    <div class="large-4 medium-6 cell margin-bottom-2">
        <div class="cell show-for-large margin-top-1"><h5><strong class="color-blue">Florianópolis</strong></h5></div>
        <div class="box-unidades">
          <div class="box-unidades-thumb">
            <img src="/img/unidade9.jpg" alt="Unidade {{name}}" title="Unidade {{name}}">
          </div>
          <div class="box-unidades-label">
            <h4>Florianópolis</h4>
            <h5>Av. Marinheiro Max Schramm, 3553 - Barreiros, Florianópolis - SC, 88095-001</h5>
          </div>
          <div class="box-unidades-info">
            
            <div class="box-unidades-info-flex">
              <div><a href="tel:08000062800"><i class="fas fa-phone"></i> 0800 006 2800</a></div>
            </div>

            <div class="box-unidades-horario">
              <div><i class="far fa-clock"></i> Segunda a sexta: das 08:00h às 19:00h</div>
              <div><i class="far fa-clock"></i> Sábado das 08:00h às 13:00</div>
            </div>

            <div class="text-center">
              <a target="_blank" class="button button-ghost" href="https://goo.gl/maps/7Nqy8jbU2CChR8me9">Saiba como chegar</a>
            </div>            

          </div>
        </div>         
      </div>   

      </div>
    </div>
  </div>
</section>

<?php 
require_once("inc/footer.php");
?>