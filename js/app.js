$(document).foundation();

  //Slider home
  var swiper1 = new Swiper('.swiper1', {
    loop: true,
    autoplay: {
      delay: 3500,
    },
    navigation: {
      nextEl: '.swiper-button-next1',
      prevEl: '.swiper-button-prev1',
    },
  });

  //swiper2
  var swiper2 = new Swiper('.swiper2', {
    slidesPerView: 3,
    spaceBetween: 30,
    loop: true,
    navigation: {
      nextEl: '.swiper-button-next2',
      prevEl: '.swiper-button-prev2',
    },
      breakpoints: {
        240: {
          slidesPerView:1.2,
        },        
        640: {
          slidesPerView:1.2,
        },
        768: {
          slidesPerView: 2,
        },
        1024: {
          slidesPerView: 3,
        },
      },        
  });

  //swiper3
  var swiper3 = new Swiper('.swiper3', {
    slidesPerView: 4,
    spaceBetween: 30,
    loop: true,
    navigation: {
      nextEl: '.swiper-button-next3',
      prevEl: '.swiper-button-prev3',
    },
      breakpoints: {
        240: {
          slidesPerView:1.2,
        },        
        640: {
          slidesPerView:1.2,
        },
        768: {
          slidesPerView: 2,
        },
        1024: {
          slidesPerView: 4,
        },
      },        
  });


  $(".wrapper-location").on("click", function(){
      var photo = "../img/"+$(this).attr("data-photo");
      var phone = $(this).attr("data-phone");
      $(".wrapper-photo-unidade img").attr("src",photo);
      $(".unidade-telefone a span").html(phone);
      $(".wrapper-location").removeClass("active");
      $(this).addClass("active");

      if(Foundation.MediaQuery.is('medium only') || Foundation.MediaQuery.is('small only')){
        $('html,body').animate({
            scrollTop: $(".wrapper-photo-unidade").offset().top
        }, 'slow');
      }

  })

$("#mobile-menu").on("click", function(){
  $(".toggle-menu").toggle();
})