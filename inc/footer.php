
  <footer>
    <div class="grid-container">
      <div class="grid-x grid-margin-x">
        <div class="large-3 medium-3 cell">
          <a href="/"><img src="/img/fredy-pneus-footer.svg" alt="Fredy Pneus" title="Fredy Pneus"></a>
        </div>

        <div class="large-3 medium-3 cell">
          <h4>Acesso rápido</h4>
          <ul>
            <li><a href="#">Serviços</a></li>
            <li><a href="#">Linha leve</a></li>
            <li><a href="#">Linha pesada</a></li>
            <li><a href="#">Rodas</a></li>
            <li><a href="#">Pneus</a></li>
          </ul>
        </div>

        <div class="large-3 medium-3 cell">
          <h4>Institucional</h4>
          <ul>
            <li><a href="#">Lojas Fredy</a></li>
            <li><a href="#">Sobre a Fredy</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Contato</a></li>
          </ul>
        </div>

        <div class="large-3 medium-3 cell">
          <div class="app-footer">
            <h4>Baixe nosso app</h4>
            <div>
              <a href="#"><img src="/img/gplay.png" alt="Google Play" title="Google Play"></a>
            </div>
            <div>
              <a href="#"><img src="/img/ios.png" alt="Apple Store" title="Apple Store"></a>
            </div>
          </div>
        </div>


      </div>
    </div>
  </footer>

  <section id="footer-bar">
    <div class="grid-container">
      <div class="grid-x grid-margin-x">
        <div class="large-4 medium-4 cell">
          <span>Redes sociais:</span>
          <a href="#"><i class="fab fa-facebook-f"></i></a>
          <a href="#"><i class="fab fa-instagram"></i></a>
        </div>
        <div class="large-8 medium-8 cell medium-text-right">
          <span>2021 | Todos os direitos reservados. Proibida reprodução total ou parcial.</span>
        </div>
      </div>
    </div>
  </section>
  
  </div><!--off-canvas-content-->

    <script src="/js/vendor/jquery.js"></script>
    <script src="/js/vendor/what-input.js"></script>
    <script src="/js/vendor/foundation.js"></script>
    <script src="/js/swiper.min.js"></script>
    <script defer src='https://www.google.com/recaptcha/api.js'></script>
    <script src="/js/lightbox.min.js"></script>
    <script src="/js/app.js"></script>
  </body>
</html>