<!doctype html>
<html class="no-js" lang="pt-BR" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fredy Pneus</title>
    <link rel="stylesheet" href="/css/foundation.min.css">
    <link rel="stylesheet" href="/css/swiper.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" crossorigin="anonymous">
    <link rel="icon" href="/img/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/img/favicon.ico" type="image/x-icon">    
    <link rel="stylesheet" href="/css/lightbox.css">
    <link rel="stylesheet" href="/css/app.css">
  </head>
  <body>

  <div class="off-canvas position-right" id="offCanvas" data-off-canvas>
    <div data-close id="closeMenuMobile"><i class="fas fa-times"></i> Fechar</div>
    <ul class="vertical menu drilldown" data-drilldown>
      <li><a href="#">Home</a></li>
      <li><a href="#">Sobre a Fredy</a></li>
      <li>
        <a href="#">Serviços</a>
        <ul class="menu vertical nested">
          <li><a href="#">Two A</a></li>
          <li><a href="#">Two B</a></li>
          <li><a href="#">Two C</a></li>
          <li><a href="#">Two D</a></li>
        </ul>
      </li>
      <li>
        <a href="#">Linha Leve</a>
        <ul class="menu vertical nested">
          <li><a href="#">Two A</a></li>
          <li><a href="#">Two B</a></li>
          <li><a href="#">Two C</a></li>
          <li><a href="#">Two D</a></li>
        </ul>
      </li>      
      <li>
        <a href="#">Linha pesada</a>
        <ul class="menu vertical nested">
          <li><a href="#">Two A</a></li>
          <li><a href="#">Two B</a></li>
          <li><a href="#">Two C</a></li>
          <li><a href="#">Two D</a></li>
        </ul>
      </li>      
      <li><a href="#">Aplicativo</a></li>
      <li><a href="#">Blog</a></li>
      <li><a href="#">Contato</a></li>
    </ul>
    
  </div>

  <div class="off-canvas-content" data-off-canvas-content>
    
  <div id="top-bar">
    <div class="grid-container">
      <div class="grid-x grid-margin-x align-middle">

        <div class="cell">

          <span><i class="fas fa-phone"></i> Televendas Fredy Pneus (47) 2105-9800 | </span> <a href="#" data-toggle="dropdown-city1" data-close-on-click="true"> Ver cidades</a>
          <div class="dropdown-pane dropdown-pane-custom top"  id="dropdown-city1" data-dropdown data-hover="true" data-hover-pane="true">
            <h6>Este telefone está disponível para as seguintes regiões</h6>
            <ul>
              <li>Koinville</li>
              <li>São bento do Sul</li>
              <li>Jaraguá do sul</li>
            </ul>
          </div>   
          <br class="hide-for-large">
          <span class="margin-left-2-custom"><i class="fas fa-phone"></i> 0800 006 2800 | </span> <a href="#" data-toggle="dropdown-city2" data-close-on-click="true"> Ver cidades</a>
          <div class="dropdown-pane dropdown-pane-custom top"  id="dropdown-city2" data-dropdown data-hover="true" data-hover-pane="true">
            <h6>Este telefone está disponível para as seguintes regiões</h6>
            <ul>
              <li>Brusque</li>
              <li>Itajaí</li>
              <li>Baln. Camboriú</li>
              <li>São José</li>
            </ul>
          </div>
        </div>
      
      </div>
    </div>
  </div>

  <header>
    <div class="grid-container">
      <div class="grid-x grid-margin-x align-middle">
          <div class="large-7 medium-10 small-10 cell">
              <div class="logo-header-wrapper">
                <div class="logo-header1">
                  <a href="/"><img src="/img/fredy-pneus.svg" alt="Fredy Pneus" title="Fredy Pneus"></a>
                </div>
                <div class="logo-header2">
                  <a href="/"><img src="/img/goodyear.svg" alt="Good Year" title="Good Year"></a>
                </div>
              </div><!--logo-header-wrapper-->
          </div>
          <div class="large-5 medium-12 cell medium-text-center mobile-order">
            <div class="display-flex-search-and-social-icons">
                <div class="wrapper-search">
                  <form data-abide novalidate action="#" method="get">
                    <input type="text" name="q" required placeholder="O que você está procurando?" id="input-search">
                    <button type="submit" id="btn-submit"><img src="/img/icon-search.svg" alt="Buscar" title="Buscar"></button>
                  </form>
                </div>
                <div class="social-block-header">
                  <a href="#"><i class="fab fa-facebook-f"></i></a>
                  <a href="#"><i class="fab fa-instagram"></i></a>
                </div>
              </div>
          </div>

          <div class="hide-for-large medium-2 small-2 cell text-right">
              <img src="/img/menu.svg" alt="Menu" title="Menu" id="mobile-menu" data-toggle="offCanvas">
          </div>
      </div>
    </div>
  </header>

  <nav>
    <div class="grid-container">
      <div class="grid-x">
        <div class="cell">
          <div class="flex-nav">
            <div>
              <ul class="dropdown menu primary-menu toggle-menu" data-dropdown-menu>
                <li><a href="#">Home</a></li>
                <li><a href="#">Sobre a Fredy</a></li>
                <li>
                  <a href="#">Serviços</a>
                  <ul class="menu">
                    <li><a href="#">Item A</a></li>
                    <li><a href="#">Item B</a></li>
                    <li><a href="#">Item C</a></li>
                  </ul>
                </li>
                <li>
                  <a href="#">Linha leve</a>
                  <ul class="menu">
                    <li><a href="#">Item A</a></li>
                    <li><a href="#">Item B</a></li>
                    <li><a href="#">Item C</a></li>
                  </ul>
                </li>   
                <li>
                  <a href="#">Linha pesada</a>
                  <ul class="menu">
                    <li><a href="#">Item A</a></li>
                    <li><a href="#">Item B</a></li>
                    <li><a href="#">Item C</a></li>
                  </ul>
                </li>                                
                <li><a href="#">Aplicativo</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="#">Contato</a></li>
              </ul>
            </div> 
            <div class="wrapper-unidades-header">
              <a href="/unidades"><i class="fas fa-map-marker-alt"></i> Unidade mais próxima de você</a>
            </div>         

          </div>

        </div>
      </div>
    </div>
  </nav>