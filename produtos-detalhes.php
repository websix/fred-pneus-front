<?php 
require_once("inc/header.php");
?>

<section id="inner-page">
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell">
        <h1>Pneus</h1>
        <ul class="breadcrumbs">
          <li><a href="#">Home</a></li>
          <li><a href="#">Pneus linha leve</a></li>
          <li><a href="#">Goodyear EfficientGrip ROF</a></li>
        </ul>        
      </div>
    </div>
  </div>
</section>

<section class="margin-top-3 margin-bottom-3">
  <div class="grid-container">
    <div class="grid-x grid-margin-x">
      <div class="large-5 medium-12 cell">
        <div class="wrapper-product-detail">
          <a href="/img/produto-detalhe.jpg" data-lightbox="gallery"><img src="/img/produto-detalhe.jpg" alt="{{name}}" title="{{name}}" class="box-service-thumb b-radius-5"></a>
          <div class="product-detail-target">
            <div class="box-product-target"><img src="/img/icon-automovel.svg" alt="Automóvel" title="Automóvel"> Ideal para: automóveis</div>
          </div>
        </div>
      </div>

      <div class="large-7 medium-12 cell">
        <h2 class="title-default color-blue margin-bottom-1">Goodyear EfficientGrip ROF</h2>
        <p>Pneu que entrega performance, design arrojado e economia de combustível para seu veículo. Economize combustível com o EfficientGrip, com a Tecnologia FuelSaving. Desfrute de viagens mais silenciosas devido ao design dos blocos que reduz os níveis de ruído.</p>
        <p>As montadoras mais reconhecidas do mercado equipam seus veículos com pneus que possuem tecnologia RunOnFlat®. Essa tecnologia permite que o pneu rode sem pressão de ar por até 80km à 80km/h. </p>
        <p>Consulte as medidas disponíveis com a tecnologia. Com estrutura leve e composto avançado da banda, o EfficientGrip necessita de menos energia para rodar.</p>

        <div class="selos-area medium-text-left text-center">
          <img src="/img/selo1.png" alt="Selo" title="Selo">
          <img src="/img/selo2.png" alt="Selo" title="Selo">
        </div>

        <div class="margin-top-1 medium-text-left text-center">
          <a href="#" class="button button-whatsapp"><i class="fab fa-whatsapp"></i> Solicitar orçamento</a>
        </div>
      </div>

    </div>

    <div class="grid-x margin-top-2">
      <div class="cell">
        <a href="#">
          <img src="/img/encontre-a-proxima-unidade.png" alt="Encontre a próxima unidade" title="Encontre a próxima unidade">
        </a>
      </div>
    </div>

  </div>
</section>

  <section class="margin-top-3 margin-bottom-3">
    <div class="bg-blue-wrapper">
    <div class="grid-container padding-right-0-mobile">
      <div class="grid-x">
        <div class="cell">
          <h2 class="title-default color-blue">Confira também outros produtos</h2>
        </div>
      </div>

        <div class="grid-x grid-margin-x">

          <div class="large-6 medium-12 cell">

            <div class="box-shortcut margin-top-2">
              <div class="box-shortcut-category-name bg-blue">
                <h2>PNEUS</h2>
                <a href="#">Ver todos</a>
              </div>
              <div class="box-shortcut-category-common">
                <a href="#">
                  <div class="box-shortcut-category-thumb"><img src="/img/icon-automovel.svg" alt="Automóvel" title="Automóvel"></div>
                  <div class="box-shortcut-category-label">Automóvel</div>
                </a>
              </div>
              <div class="box-shortcut-category-common">
                <a href="#">
                  <div class="box-shortcut-category-thumb"><img src="/img/icon-suv.svg" alt="SUV/Utilitários" title="SUV/Utilitários"></div>
                  <div class="box-shortcut-category-label">SUV/Utilitários</div>
                </a>              
              </div>
              <div class="box-shortcut-category-common">
                <a href="#">
                  <div class="box-shortcut-category-thumb"><img src="/img/icon-pesada.svg" alt="Linha pesada" title="Linha pesada"></div>
                  <div class="box-shortcut-category-label">Linha pesada</div>
                </a>               
              </div>
            </div>

          </div>

          <div class="large-6 medium-12 cell">
            
            <div class="box-shortcut margin-top-2">
              <div class="box-shortcut-category-name bg-green">
                <h2>RODAS</h2>
                <a href="#">Ver todos</a>
              </div>
              <div class="box-shortcut-category-common">
                <a href="#">
                  <div class="box-shortcut-category-thumb"><img src="/img/icon-rodas-esportivas.svg" alt="Esportivas" title="Esportivas"></div>
                  <div class="box-shortcut-category-label">Esportivas</div>
                </a>
              </div>
              <div class="box-shortcut-category-common">
                <a href="#">
                  <div class="box-shortcut-category-thumb"><img src="/img/icon-rodas-originais.svg" alt="Originais" title="Originais"></div>
                  <div class="box-shortcut-category-label">Originais</div>
                </a>              
              </div>
              <div class="box-shortcut-category-common">
                <a href="#">
                  <div class="box-shortcut-category-thumb"><img src="/img/icon-rodas-seminovas.svg" alt="Semi-novas" title="Semi-novas"></div>
                  <div class="box-shortcut-category-label">Semi-novas</div>
                </a>               
              </div>
            </div>

          </div>

        </div>
      </div>
  </section>

  <section class="margin-top-2 margin-bottom-3">
    <div class="grid-container padding-right-0-mobile">
      <div class="grid-x">
        <div class="cell">
            <div class="flex-icon-title-label">
              <h2 class="title-default">Produtos relacionados</h2>
            </div>
        </div>
      </div>

      <div class="grid-x grid-margin-x">

        <div class="cell relative">
          <div class="swiper-button-next swiper-button-next3"><img src="/img/right2.svg" alt="Right" title="Avançar"></div>
          <div class="swiper-button-prev swiper-button-prev3"><img src="/img/left2.svg" alt="Left" title="Voltar"></div>

          <div class="swiper-container swiper3 padding-top-2">
            <div class="swiper-wrapper">
              
              <div class="swiper-slide">
                <div class="box-product">
                  <div class="box-product-thumb"><img src="/img/product1.png" alt="{{name}}" title="{{name}}"></div>
                  <div class="box-product-title"><h3>Goodyear Cargo Marathon 2</h3></div>
                  <div class="box-product-info"><p>Composto especial com sílica na banda de rodagem e construção otimizada proporcionam menor resistência ao rolamento.</p></div>
                  <div class="box-product-target"><img src="/img/icon-automovel.svg" alt="Automóvel" title="Automóvel"> Ideal para: automóveis</div>
                </div>
              </div> 

              <div class="swiper-slide">
                <div class="box-product">
                  <div class="box-product-thumb"><img src="/img/product2.png" alt="{{name}}" title="{{name}}"></div>
                  <div class="box-product-title"><h3>Goodyear Wrangler SUV</h3></div>
                  <div class="box-product-info"><p>Entrega maior durabilidade e controle através de seu design superior e construção otimizada. Melhor aderência em piso molhado.</p></div>
                  <div class="box-product-target"><img src="/img/icon-suv.svg" alt="SUV/Utilitários" title="SUV/Utilitários"> Ideal para: SUV/Utilitários</div>
                </div>
              </div> 

              <div class="swiper-slide">
                <div class="box-product">
                  <div class="box-product-thumb"><img src="/img/product3.png" alt="{{name}}" title="{{name}}"></div>
                  <div class="box-product-title"><h3>Kelly Edge SUV</h3></div>
                  <div class="box-product-info"><p>Desenvolvido para Pick-ups e SUVs. Seu baixo desgaste da banda de rodagem entrega alta quilometragem e economia no seu dia-a-dia.</p></div>
                  <div class="box-product-target"><img src="/img/icon-suv.svg" alt="SUV/Utilitários" title="SUV/Utilitários"> Ideal para: SUV/Utilitários</div>
                </div>
              </div> 
              
              <div class="swiper-slide">
                <div class="box-product">
                  <div class="box-product-thumb"><img src="/img/product1.png" alt="{{name}}" title="{{name}}"></div>
                  <div class="box-product-title"><h3>G677 OTR</h3></div>
                  <div class="box-product-info"><p>Transporta até 6 mil toneladas a mais. Recomendado para eixos de tração.</p></div>
                  <div class="box-product-target"><img src="/img/icon-pesada.svg" alt="Linha pesada" title="Linha pesada"> Ideal para: Linha pesada</div>
                </div>
              </div> 

            </div>
          </div>          
        </div>

      </div>

      <div class="grid-x margin-top-2 text-center">
        <div class="cell">
          <a class="button button-ghost" href="#">Confira todos os serviços</a>
        </div>
      </div>      

    </div>
  </section>



<?php 
require_once("inc/footer.php");
?>