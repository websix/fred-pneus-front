<?php 
require_once("inc/header.php");
?>

<section id="inner-page">
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell">
        <h1>Nossos serviços</h1>
        <ul class="breadcrumbs">
          <li><a href="#">Home</a></li>
          <li><a href="#">Nossos serviços</a></li>
          <li><a href="#">Higienização do ar condicionado por oxi sanitização</a></li>
        </ul>        
      </div>
    </div>
  </div>
</section>

<section class="margin-top-3 margin-bottom-3">
  <div class="grid-container">
    <div class="grid-x grid-margin-x">
      <div class="large-5 medium-5 cell">
        <img src="/img/higienizacao.jpg" alt="Higienização" title="Higienização" class="box-service-thumb b-radius-5">
      </div>

      <div class="large-7 medium-7 cell">
        <h2 class="title-default color-blue margin-bottom-1">Higienização do ar condicionado por oxi sanitização</h2>
        <p>Oxi Sanitização é basicamente um processo realizado através de um máquina que produz o gás ozônio a partir do oxigênio.</p>
        <p>Este gás, natural e presente na atmosfera, tem efeito germicida e é eficiente na destruição de vírus, bactérias e fungos.</p>
        <p>É muito eficiente na limpeza do ar-condicionado automotivo e consequentemente na remoção do mau cheiro na parte interna do veículo, eliminando as bactérias e os microrganismos. Após todo o processo de limpeza, o ozônio se converte em oxigênio novamente.</p>
        <p>A Oxi Sanitização é utilizada também na medicina e odontologia, por ser uma tecnologia totalmente limpa, que dispensa o uso de produtos químicos.</p>
        <p>Desta maneira, a oxi-sanitização é aplicada para deixar o ar de seu veículo muito mais limpo e puro. No caso específico da limpeza e purificação do ar-condicionado, a oxi-sanitização vem se mostrando como o método conhecido mais eficiente, seguro e efetivo.</p>
      </div>

    </div>

    <div class="grid-x margin-top-2">
      <div class="cell">
        <a href="#">
          <img src="/img/encontre-a-proxima-unidade.png" alt="Encontre a próxima unidade" title="Encontre a próxima unidade">
        </a>
      </div>
    </div>

  </div>
</section>

  <section class="margin-top-3">
    <div class="grid-container padding-right-0-mobile">
      <div class="grid-x">
        <div class="cell">
          <h2 class="title-default">Confira outros serviços</h2>
          <h2 class="sub-title">Oferecemos uma gama completa de serviços especializados para você. Confira:</h2>
        </div>
      </div>

      <div class="grid-x margin-top-1">

        <div class="cell relative">
          <div class="swiper-button-next swiper-button-next2"><img src="/img/right2.svg" alt="Right" title="Avançar"></div>
          <div class="swiper-button-prev swiper-button-prev2"><img src="/img/left2.svg" alt="Left" title="Voltar"></div>

          <div class="swiper-container swiper2">
            <div class="swiper-wrapper">
              
              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service2.jpg" alt="Serviço especializado de troca de óleo na Fredy" title="Serviço especializado de troca de óleo na Fredy" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service2.svg" alt="Serviço especializado de troca de óleo na Fredy" title="Serviço especializado de troca de óleo na Fredy" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Serviço especializado de troca de óleo na Fredy</h2></div>
                      </div>
                  </div>
                </a>              
              </div>

              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service3.jpg" alt="Revisão e a manutenção da suspensão de veículos" title="Revisão e a manutenção da suspensão de veículos" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service3.svg" alt="Revisão e a manutenção da suspensão de veículos" title="Revisão e a manutenção da suspensão de veículos" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Revisão e a manutenção da suspensão de veículos</h2></div>
                      </div>
                  </div>
                </a>              
              </div>              
                    
              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service4.jpg" alt="Atenção com os pneus desgastados irregularmente" title="Atenção com os pneus desgastados irregularmente" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service4.svg" alt="Atenção com os pneus desgastados irregularmente" title="Atenção com os pneus desgastados irregularmente" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Atenção com os pneus desgastados irregularmente</h2></div>
                      </div>
                  </div>
                </a>              
              </div> 

              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service5.jpg" alt="O serviço ideal para manter suas rodas sempre novas." title="O serviço ideal para manter suas rodas sempre novas." class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service5.svg" alt="O serviço ideal para manter suas rodas sempre novas." title="O serviço ideal para manter suas rodas sempre novas." class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>O serviço ideal para manter suas rodas sempre novas.</h2></div>
                      </div>
                  </div>
                </a>              
              </div>

              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service6.jpg" alt="Manutenção e cuidados necessários com os freios" title="Manutenção e cuidados necessários com os freios" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service6.svg" alt="Manutenção e cuidados necessários com os freios" title="Manutenção e cuidados necessários com os freios" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Manutenção e cuidados necessários com os freios</h2></div>
                      </div>
                  </div>
                </a>              
              </div> 

              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service7.jpg" alt="Alinhamento e balanceamento de Pneus" title="Alinhamento e balanceamento de Pneus" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service7.svg" alt="Alinhamento e balanceamento de Pneus" title="Alinhamento e balanceamento de Pneus" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Alinhamento e balanceamento de Pneus</h2></div>
                      </div>
                  </div>
                </a>              
              </div>

              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service8.jpg" alt="Conserto de Pneus" title="Conserto de Pneus" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service8.svg" alt="Conserto de Pneus" title="Conserto de Pneus" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Conserto de Pneus</h2></div>
                      </div>
                  </div>
                </a>              
              </div>

              <div class="swiper-slide">
                <a href="#">
                  <div class="box-service">
                      <img src="/img/service9.jpg" alt="Mantenha o seu carro sempre bem equilibrado" title="Mantenha o seu carro sempre bem equilibrado" class="box-service-thumb">
                      <div class="box-service-info">
                        <div class="box-service-icon">
                          <img src="/img/service9.svg" alt="Mantenha o seu carro sempre bem equilibrado" title="Mantenha o seu carro sempre bem equilibrado" class="box-service-icon">
                        </div>
                        <div class="box-service-label"><h2>Mantenha o seu carro sempre bem equilibrado</h2></div>
                      </div>
                  </div>
                </a>              
              </div>                    

            </div>
          </div> 

        </div>

      </div>

      <div class="grid-x margin-top-2 text-center margin-bottom-3">
        <div class="cell margin-top-1 margin-bottom-1">
          <a class="button button-ghost" href="#">Confira todos os serviços</a>
        </div>
      </div>

    </div>
  </section>


<?php 
require_once("inc/footer.php");
?>