<?php 
require_once("inc/header.php");
?>

<section id="inner-page">
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell">
        <h1>Blog</h1>
        <ul class="breadcrumbs">
          <li><a href="#">Home</a></li>
          <li><a href="#">Blog</a></li>
        </ul>        
      </div>
    </div>
  </div>
</section>

<section class="padding-top-3 padding-bottom-3 bg-grey">
    <div class="grid-container">

      <div class="grid-x grid-margin-x margin-top-1">

        <div class="large-4 medium-6 cell margin-bottom-3">
          <div class="card-blog">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog1.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>
        </div>     

        <div class="large-4 medium-6 cell margin-bottom-3">
          <div class="card-blog">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog2.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>
        </div>  

        <div class="large-4 medium-6 cell margin-bottom-3">
          <div class="card-blog">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog3.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>
        </div>

        <div class="large-4 medium-6 cell margin-bottom-3">
          <div class="card-blog">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog1.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>
        </div>     

        <div class="large-4 medium-6 cell margin-bottom-3">
          <div class="card-blog">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog2.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>
        </div>  

        <div class="large-4 medium-6 cell margin-bottom-3">
          <div class="card-blog">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog3.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>
        </div>

        <div class="large-4 medium-6 cell margin-bottom-3">
          <div class="card-blog">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog1.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>
        </div>     

        <div class="large-4 medium-6 cell margin-bottom-3">
          <div class="card-blog">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog2.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>
        </div>  

        <div class="large-4 medium-6 cell margin-bottom-3">
          <div class="card-blog">
            <div class="card-blog-thumb">
              <a href="#"><img src="/img/blog3.jpg" alt="{{name}}" title="{{name}}"></a>
            </div>
            <div class="card-blog-title">
              <div class="card-blog-date"><i class="far fa-calendar-alt"></i> 10/01/2021</div>
              <a href="#">
                <h2>Quando devo trocar os pneus do meu carro?</h2>
                <span>» Leia mais</span>
              </a>
            </div>
          </div>
        </div>

      </div>



    </div>
</section>

<?php 
require_once("inc/footer.php");
?>